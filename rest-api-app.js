const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const memory = require('feathers-memory');

const app = express(feathers());

// Turn on JSON body parsing for REST services
app.use(express.json())
// Turn on URL-encoded body parsing for REST services
app.use(express.urlencoded({ extended: true }));
// Set up REST transport using Express
app.configure(express.rest());

// Initialize the messages service
app.use('messages', memory({
    paginate: {
        default: 10,
        max: 25
    }
}));

const convertQueryParams = async context => {
    console.log(context.params.query);
    context.params.query.counter = parseInt(context.params.query.counter, 10);
    return context;
};

app.service('messages').hooks({
    before: {
        find: convertQueryParams,
    }
});

async function createAndFind() {
    // Stores a reference to the messages service so we don't have to call it all the time
    const messages = app.service('messages');

    for(let counter = 0; counter < 100; counter++) {
        await messages.create({
            counter,
            message: `Message number ${counter}`
        });
    }
}

createAndFind();

// Set up an error handler that gives us nicer errors
app.use(express.errorHandler());

// Start the server on port 3030
const server = app.listen(3030);

//// Use the service to create a new message on the server
//app.service('messages').create({
//     text: 'Hello from the server'
// });

server.on('listening', () => console.log('Feathers REST API started at localhost:3030'));